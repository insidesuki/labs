<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231004045700 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE event_store (id_event_store VARCHAR(255) NOT NULL, occurred_on DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', event_name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, arguments LONGTEXT DEFAULT NULL, PRIMARY KEY(id_event_store)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invoices (id VARCHAR(36) NOT NULL, number VARCHAR(100) NOT NULL, amount INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE log_api_calls (id_api_event VARCHAR(255) NOT NULL, app_name VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, scope VARCHAR(255) NOT NULL, method VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, response VARCHAR(255) NOT NULL, finishDate DATETIME DEFAULT NULL, start_time DATETIME NOT NULL, payload VARCHAR(255) DEFAULT NULL, username VARCHAR(255) DEFAULT NULL, ip_origin VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id_api_event)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE logs (id_log VARCHAR(36) NOT NULL, type VARCHAR(100) NOT NULL, id_user VARCHAR(36) NOT NULL, log_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', remote_address VARCHAR(50) NOT NULL, user_agent LONGTEXT NOT NULL, details LONGTEXT DEFAULT NULL, PRIMARY KEY(id_log)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profile (profile VARCHAR(255) NOT NULL, default_route VARCHAR(100) NOT NULL, base_template VARCHAR(100) NOT NULL, PRIMARY KEY(profile)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id VARCHAR(255) NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (profile VARCHAR(255) DEFAULT NULL, username VARCHAR(100) NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(200) NOT NULL, active TINYINT(1) NOT NULL, is_verified TINYINT(1) NOT NULL, email VARCHAR(200) NOT NULL, roles JSON NOT NULL, last_password_verification DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', idUser VARCHAR(255) NOT NULL, INDEX IDX_1483A5E98157AA0F (profile), PRIMARY KEY(idUser)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748ABF396750 FOREIGN KEY (id) REFERENCES users (idUser)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E98157AA0F FOREIGN KEY (profile) REFERENCES profile (profile)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748ABF396750');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E98157AA0F');
        $this->addSql('DROP TABLE event_store');
        $this->addSql('DROP TABLE invoices');
        $this->addSql('DROP TABLE log_api_calls');
        $this->addSql('DROP TABLE logs');
        $this->addSql('DROP TABLE profile');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE users');
    }
}
