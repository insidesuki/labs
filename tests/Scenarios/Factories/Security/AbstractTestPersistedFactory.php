<?php
declare(strict_types=1);

namespace App\Tests\Scenarios\Factories\Security;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AbstractTestPersistedFactory extends KernelTestCase
{

    protected static function getService(string $service): mixed
    {

        $conta = null;

        if(null === $kernel){
            echo 'es null, init';
            $kernel = self::bootKernel();
        }


        return $kernel->getContainer()->get($service);

    }

}