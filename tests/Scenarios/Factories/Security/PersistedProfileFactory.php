<?php
declare(strict_types=1);

namespace App\Tests\Scenarios\Factories\Security;

use Security\Domain\Profile\Command\CreateProfileCommandInterface;
use Security\Domain\Profile\Entity\Profile;
use Security\Infrastructure\Storage\Doctrine\Repository\ProfileDoctrineRepository;

class PersistedProfileFactory extends AbstractTestPersistedFactory
{


    public static function create(?CreateProfileCommandInterface $createProfileCommand = null): Profile
    {

        return self::getService(ProfileDoctrineRepository::class)
            ->store(
                ProfileFactory::create($createProfileCommand)
            );


    }
}