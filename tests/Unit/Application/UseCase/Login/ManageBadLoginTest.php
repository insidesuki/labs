<?php

declare(strict_types=1);

namespace App\Tests\Unit\Application\UseCase\Login;

use App\Tests\Scenarios\Factories\Security\UserFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Security\Application\Login\ManageBadLogin;
use Security\Application\Login\RegisterLoginError;
use Security\Domain\Log\Repository\LogRepository;
use Security\Domain\User\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class ManageBadLoginTest extends TestCase
{

    private ManageBadLogin $manageBadLogin;

    private UserRepository|MockObject $userMockRepo;
    private string $username;
    private string $ipAddress;
    private string $userAgent;

    private LogRepository|MockObject $logMockRepo;

    public function setUp(): void
    {
        $this->logMockRepo = $this->createMock(LogRepository::class);
        $this->userMockRepo = $this->createMock(UserRepository::class);
        $this->registerLogMock = $this->createMock(RegisterLoginError::class);
        $this->manageBadLogin = new ManageBadLogin($this->logMockRepo, $this->userMockRepo, $this->registerLogMock);
        $this->username = 'test';
        $this->ipAddress = '127.0.0.1';
        $this->userAgent = 'test';
    }


    public function testReturnFalseWhenNotExistsUser(): void
    {

        $result = $this->manageBadLogin->handle($this->username, $this->ipAddress, $this->userAgent);
        $this->assertFalse($result);

    }


    public function testReturnTrueAndBlockUserWhenExistsUser(): void
    {

        $user = UserFactory::create($this->createMock(UserPasswordHasher::class));
        $this->userMockRepo->method('searchByUsername')->willReturn($user);

        $this->logMockRepo->method('countRecentLoginAttemptsUser')->willReturn(10);
        $result = $this->manageBadLogin->handle($this->username, $this->ipAddress, $this->userAgent);
        $this->assertTrue($result);
        $this->assertFalse($user->isActive());

    }

}
