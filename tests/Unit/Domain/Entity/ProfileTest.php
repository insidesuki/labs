<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Entity;

use App\Tests\Scenarios\Factories\Security\ProfileFactory;

use PHPUnit\Framework\TestCase;

class ProfileTest extends TestCase
{

    public function testOkCreateProfile():void{


        $profile = ProfileFactory::create();
        $command = ProfileFactory::$command;
        $this->assertSame($profile->profile,$command->profile());
        $this->assertSame($profile->defaultRoute(),$command->defaultRoute());
        $this->assertSame($profile->baseTemplate(),$command->baseTemplate());
        $this->assertSame($profile->__toString(),$command->profile());

    }


}
