<?php
declare(strict_types=1);

namespace App\Tests\Application\UseCase\ListUser;

use Admin\Infrastructure\Web\User\Presentation\ListUsersArrayPresentation;
use App\Tests\Scenarios\Factories\Security\UserFactory;
use PHPUnit\Framework\TestCase;
use Security\Application\User\ListUser\ListUsers;
use Security\Domain\User\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class ListUsersTest extends TestCase
{

	public function setUp(): void
	{
		$this->passwordHasher = $this->createMock(UserPasswordHasher::class);
		$this->user = UserFactory::create($this->passwordHasher);
	}


	public function testOkListUsers():void{

		$userMockRepo = $this->createMock(UserRepository::class);
		$expectedResult = [$this->user,$this->user];

		$userMockRepo->method('findAll')->willReturn($expectedResult);

		$listUsers = new ListUsers($userMockRepo);
		$presentation = $listUsers->handle(new ListUsersArrayPresentation());

		$this->assertCount(count($expectedResult),$presentation->read());





	}

}
