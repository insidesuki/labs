<?php
declare(strict_types=1);

namespace App\Tests\Functional\Application\UseCase\Login;

use App\Tests\Scenarios\Factories\Security\PersistedUserFactory;
use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractIntegrationTest;
use Security\Application\Login\ManageBadLogin;
use Security\Domain\Log\Entity\Log;
use Security\Infrastructure\Storage\Doctrine\Repository\LogDcmRepository;

class ManageBadLoginTest extends AbstractIntegrationTest
{


    private ManageBadLogin $manageBadLogin;
    private string $username;
    private string $ipAddress;
    private string $userAgent;

    public function setUp(): void
    {
        parent::setUp();
        $this->manageBadLogin = $this->getService(ManageBadLogin::class);
        $this->username = 'test';
        $this->ipAddress = '127.0.0.1';
        $this->userAgent = 'test';
    }


    public function testReturnFalseWhenNotExistsUser(): void
    {

        $result = $this->manageBadLogin->handle($this->username, $this->ipAddress, $this->userAgent);
        $this->assertFalse($result);

    }

    public function testReturnFalseWhenExistsUserButMustNoBlocked(): void
    {

        $user = PersistedUserFactory::create($this->containerTest);
        $user->activate();
        $result = $this->manageBadLogin->handle($this->username, $this->ipAddress, $this->userAgent);
        $this->assertFalse($result);
        $this->assertTrue($user->isActive());

    }


    public function testReturnTrueWhenExistsUserAndMustBeBlocked(): void
    {


        $user = PersistedUserFactory::create($this->containerTest);
        $user->activate();

        // create 5 login error for user
        $logRepo = $this->getService(LogDcmRepository::class);

        for ($i = 1; $i <= 6; $i++) {
            $error1 = Log::createLoginError($user->idUser(), '1', '2');
            $logRepo->store($error1);
        }


        $result = $this->manageBadLogin->handle($user->getUserIdentifier(), $this->ipAddress, $this->userAgent);
        $this->assertTrue($result);
        $this->assertFalse($user->isActive());

    }





}
