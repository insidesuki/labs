<?php
declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Storage\Doctrine\Repository;

use Billing\Infrastructure\Storage\Doctrine\Repository\InvoiceDcmRepository;
use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractIntegrationTest;

class InvoiceDcmRepositoryTest extends AbstractIntegrationTest
{


	public function testFindInvoice():void{

		$repo = $this->getInvoiceRepo();

		$id = 'invoice_1';

		$invoice = $repo->findInvoice($id);
		$originalTotal = $invoice->total()->amount;
		$invoice->increment(200);

		$this->assertSame($originalTotal + 200,$invoice->total->amount);


	}


	private function getInvoiceRepo():InvoiceDcmRepository{
		return $this->getService(InvoiceDcmRepository::class);
	}

}
