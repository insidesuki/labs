<?php
declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Storage\Doctrine\Repository;

use App\Tests\Scenarios\Factories\Security\ProfileFactory;
use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractIntegrationTest;
use Security\Domain\Profile\Entity\Profile;
use Security\Infrastructure\Storage\Doctrine\Repository\ProfileDoctrineRepository;

class ProfileDoctrineRepositoryTest extends AbstractIntegrationTest
{

    private ProfileDoctrineRepository $profileRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->profileRepo = $this->getService(ProfileDoctrineRepository::class);
    }


    public function testOkSearchByProfile(): void
    {


        $newProfile = ProfileFactory::create();
        $this->profileRepo->store($newProfile);

        $profile = $this->profileRepo->searchByProfile($newProfile->profile);

        $this->assertInstanceOf(Profile::class, $profile);


    }

}
