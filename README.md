**InsideSuki-Labs**
==============================

Hexagonal architecture for SF-5.4 and PHP > 8.1.

It is about a customization of symfony 5.4.
The kernel lives in /src, isolated from your application code which lives in the /app folder.

**Features**
==

- Authentification system (dicoupled)
- Restore password  (dicoupled)
- API
- Monolog integration
- PlaniAdmin Template integration

**Installation**
==============================

- composer install
- create a .env.local stored in (config/env folder), setup your db connection, an mailer DSN
- run migrations.

**Routes**
===========
- /login
- /api/docs

**TEAM AGREEMENTS**
  ==

---

**TESTS**

- Intentar testear todo unitariamente
- Poner en los comentarios de las clases de test el group que pertenece, de esta manera podemos pasar solo los unitarios o los integracion, ej:
```
/**
 * @group unit
 */
class ObtenerServicioComponenteTest
```
>> - unit: test unitarios
>> - integration: tests de integracion
>> - e2e: tests end to end (controllers, web, etc)
- Testear todo desde Application hasta domain
- 100 % covertura en Application-Domain
- Utilizar repositorio en memorya para los casos de usos o mock si hacen falta.
- Tests de Integración cuando es necesario
- La carpeta Test esta divido en Unit,E2E,Functional
**IDIOMA**
- Se programa en ingles
- Lenguaje ubicuo , todos los casos de usos, todo lo que negocio habla en CASTELLANO
- Metodos privados internos

**PREFIJOS DE CLASES,REPOS, ETC**
- **Controllers:** Action, ej: SendEmailAction
- **Repositorios:** Para Doctrine Dcm, ej: UserDcmRepository, Memory, ej: UserMemoryRepository
- **Interfaces:** Interface, ej: UserInterface,UserRepositoryInterface
- **Commands:** Command, eh: CreateClienteCommand

**ENTIDADES**

- Toda la lógica posible tiene que vivir en las entidades (Entidades ricas NO ANEMICAS)
- Si la logica de negocio de la entidad es sencilla, no complicarlo
- Si para crear el objeto se necesita realizar comprobaciones para que el objeto se cree en un estado consistente, utilizar el patron self constructor (constructora privada con metodo estatico).
  Realizar las comprobaciones necesarias antes de crear el objeto
- Todos los atributos privados
- NO utilizar setters publicos
- Utilizar lenguaje ubicuo


**REPOSITORIOS DOCTRINE**

- Todos los repositorios tienen que extender de Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository, nunca de EntityRepository, ya que de esta manera no nos acoplamos a
  doctrine
- Los metodos Store, tienen que devolver siempre la Entidad sobre el cual esta operando
- Tener un metodo findOrFail, por ejemplo para buscar una entidad, que siempre devuelva una entidad, sino que lanze una excepcion de dominio.
- Search puede devolver null
- Find siempre devuelve una entidad

**TEMPLATES TWIG**

- Las vistas viven en {NOMBRE_MODULO}/Infrastructure/Web/Gui/Twig
- Nombre de las plantillas snake_case
- Seguir el coding standard https://twig.symfony.com/doc/3.x/coding_standards.html
- Cada modulo tiene su BaseTemplate.html.twig, de esta manera podemos personalizar la vista para cada perfil de usuario: Admin,Alumno,Responsable,Formador, etc
- El template utilizado es  https://bootstrapmade.com/demo/NiceAdmin/ , utilizar de guia, esta basado en bootstrap 5


**RUTAS**


- Mapeo en yaml NO ANNOTATIONS
- Los archivos de rutas viven en {NOMBRE_MODULO}/Infrastructure/config con el nombre de {module}_routes.yaml
- Para que cargue la ruta del modulo, importar en config/routes.yaml
- El nombre de la ruta tiene que estar separado con guiones bajo ej:

 ```
    app_logout:
        path: /logout
        controller: Application\Security\Infrastructure\Controller\SecurityController::login
 ```

- El path de las rutas es SUMAMENTE IMPORTANTE, ya que mediante el PATH base, el firewall de Symfony denegará acceso o no, es decir si vamos a realizar una ruta para el dashboard de alumno,
  deberia de quedar asi:

  ```  
     alumno_dashbard:
        path: /alumno/dashboard
        controller: FQNS...
  ```
  De esta manera todo lo que empiece por /alumno, el firewall sabe que solo lo puede ver un usuario con el rol de ALUMNO.


**SERVICES**

- Las configuraciones del service container de Symfony viven en {NOMBRE_MODULO}/Infrastructure/config/{nombre_modulo}_services.yaml
- Para que cargue el archivo importar en el config/services.yaml


