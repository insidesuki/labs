<?php
declare(strict_types = 1);

namespace Shared\Domain\Exception;
use RuntimeException;

class ApiEventDoesNotExistsException extends RuntimeException
{

}