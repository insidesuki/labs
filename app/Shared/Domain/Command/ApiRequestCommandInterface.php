<?php

namespace Shared\Domain\Command;
interface ApiRequestCommandInterface
{

	public function url():string;

	public function appName():string;

	public function scope():string;

	public function method():string;

	public function payload():string;

	public function username():string;

	public function ipOrigin():string;


}