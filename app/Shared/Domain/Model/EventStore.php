<?php
declare(strict_types = 1);

namespace Shared\Domain\Model;

use DateTimeImmutable;
use Symfony\Component\Uid\Uuid;

class EventStore
{

	public readonly string    $idEventStore;
	private DateTimeImmutable $occurredOn;
	private string            $eventName;
	private string            $description;
	private ?string           $arguments;

	/**
	 * @param DateTimeImmutable $ocurredOn
	 * @param string $eventName
	 * @param string $arguments
	 */
	public function __construct(DateTimeImmutable $ocurredOn, string $eventName, string $description, ?string $arguments = null)
	{
		$this->idEventStore = Uuid::v4()->toRfc4122();
		$this->occurredOn   = $ocurredOn;
		$this->eventName    = $eventName;
		$this->description  = $description;
		$this->arguments    = $arguments;
	}

	public function occurredOn(): DateTimeImmutable
	{
		return $this->occurredOn;
	}

	public function description(): ?string
	{
		return $this->description;
	}


	public function eventName(): string
	{
		return $this->eventName;
	}

	public function arguments(): string
	{
		return $this->arguments;
	}


}