<?php
declare(strict_types = 1);

namespace Shared\Domain\Repository;

use Shared\Domain\Model\EventStore;

interface EventStoreRepositoryInterface
{

	public function store(EventStore $eventStore): void;

}