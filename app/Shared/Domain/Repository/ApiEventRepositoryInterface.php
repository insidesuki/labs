<?php

namespace Shared\Domain\Repository;
use Shared\Domain\Model\ApiEvent;

interface ApiEventRepositoryInterface
{
	public function findApiEvent(string $idLogEvent):ApiEvent;

	public function store(ApiEvent $apiEvent):ApiEvent;

}