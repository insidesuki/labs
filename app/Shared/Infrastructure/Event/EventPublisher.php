<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Event;
use Insidesuki\DDDUtils\Domain\Event\Contracts\EventPersistibleInterface;
use Insidesuki\DDDUtils\Domain\Event\DomainEventPublisher;
use JsonException;
use Shared\Domain\Model\EventStore;
use Shared\Domain\Repository\EventStoreRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventPublisher
{

	public function __construct(
		private readonly EventDispatcherInterface $eventDispatcher,
		private readonly EventStoreRepositoryInterface $eventStoreRepository
	){

	}

	/**
	 * @throws JsonException
	 */
	public function __invoke(): void
	{
		// get all events
		$events = DomainEventPublisher::instance()->popEvents();


		foreach ($events as $event) {

			if($event instanceof EventPersistibleInterface){
				// store events
				$this->eventStoreRepository->store(
					new EventStore(
						$event->occurredOn(),
						$event->name(),
						json_encode($event, JSON_THROW_ON_ERROR)
					)
				);
			}

			// dispatch event
			$this->eventDispatcher->dispatch($event, $event::NAME);
		}
	}

}