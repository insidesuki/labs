<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Command;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

/**
 * run command in background
 */
class RunCommandAsync
{

	public function __construct(private KernelInterface $kernel){

	}

	public function run(string $command):void{

		$process = Process::fromShellCommandline($command);
		$process->setWorkingDirectory($this->kernel->getProjectDir());
		$process->disableOutput();
		$process->setTimeout(0);
		$process->start();

	}

}