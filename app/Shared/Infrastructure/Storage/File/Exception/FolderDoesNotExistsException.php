<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Storage\File\Exception;
use RuntimeException;

class FolderDoesNotExistsException extends RuntimeException
{

}