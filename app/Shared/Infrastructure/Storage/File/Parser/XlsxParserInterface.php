<?php

namespace Shared\Infrastructure\Storage\File\Parser;
interface XlsxParserInterface
{

	public function getNumRows():int;

	public function parse(string $xlsxFile);

	public function getRows(): array;

	public function getTitles():array;

}