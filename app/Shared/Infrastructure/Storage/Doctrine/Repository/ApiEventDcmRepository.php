<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Storage\Doctrine\Repository;
use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use Shared\Domain\Exception\ApiEventDoesNotExistsException;
use Shared\Domain\Model\ApiEvent;
use Shared\Domain\Repository\ApiEventRepositoryInterface;

class ApiEventDcmRepository extends AbstractDoctrineRepository implements ApiEventRepositoryInterface
{

	/**
	 * @inheritDoc
	 */
	protected static function entityClass(): string
	{
		return ApiEvent::class;
	}

	public function findApiEvent(string $idLogEvent): ApiEvent
	{
		$apiEvent = $this->findById($idLogEvent);
		if(null === $apiEvent){
			throw new ApiEventDoesNotExistsException($idLogEvent);
		}

		return $apiEvent;
	}

	public function store(ApiEvent $apiEvent): ApiEvent
	{
		$this->saveEntity($apiEvent);
		return $apiEvent;
	}
}