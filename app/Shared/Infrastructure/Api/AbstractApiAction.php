<?php
declare(strict_types=1);

namespace Shared\Infrastructure\Api;

use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AbstractApiAction extends AbstractCommonAction
{


    protected function apiResponse($message, $code = 200): JsonResponse
    {
        return new JsonResponse($message, $code);
    }

    protected function created(string $message): JsonResponse
    {
        $response = 'Resource created. #ID:' . $message;
        return $this->apiResponse($response, Response::HTTP_CREATED);
    }

    protected function exception(string $message): JsonResponse
    {
        return $this->apiResponse($message, Response::HTTP_BAD_REQUEST);
    }

    protected function updated(string $message = 'Updated'): JsonResponse
    {
        return $this->apiResponse($message, 200);
    }

    protected function notFound(string $message = 'Resource not found!'): JsonResponse
	{
        return $this->apiResponse($message, 404);
    }

    protected function responseData(array $data): JsonResponse
	{
        return $this->apiResponse($data, 200);
    }


}