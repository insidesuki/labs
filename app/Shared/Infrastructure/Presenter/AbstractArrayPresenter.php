<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Presenter;
abstract class AbstractArrayPresenter
{

	protected array $data = [];

	public function read(): array
	{
		return $this->data;
	}

}