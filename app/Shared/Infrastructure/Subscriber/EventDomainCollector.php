<?php
declare(strict_types = 1);

namespace Shared\Infrastructure\Subscriber;

use JsonException;
use Shared\Infrastructure\Event\EventPublisher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class EventDomainCollector implements EventSubscriberInterface
{

	public function __construct(
		private readonly EventPublisher $eventPublisher
	){}

	public static function getSubscribedEvents(): array
	{
		return [
			KernelEvents::TERMINATE => 'onKernelTerminate'
		];
	}

	/**
	 * @throws JsonException
	 */
	public function onKernelTerminate(): void
	{

		$this->eventPublisher->__invoke();


	}
}