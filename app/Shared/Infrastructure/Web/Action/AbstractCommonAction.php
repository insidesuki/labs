<?php

namespace Shared\Infrastructure\Web\Action;

use InvalidArgumentException;
use RuntimeException;
use Security\Domain\Profile\Entity\Profile;
use Shared\Application\Service\Report\Contract\ReportGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;
use function count;

abstract class AbstractCommonAction extends AbstractController
{


    /**
     *
     * @param Request $request
     * @return array
     */
    protected function getJsonDataAsArray(Request $request): array
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if (count($data) === 0) {
            throw new InvalidArgumentException('Empty JsonBody');
        }

        return $data;
    }

    /**
     * Validate a dto using Validator
     * @param object $dto
     * @return void
     * @TODO , for some estrange reason, the violations collections repeats, not uniques
     * violations
     */
    protected function validateDto(object $dto,ValidatorInterface $validator): void
    {
        // validate dto
        $violations = $validator->validate($dto);

        if (count($violations) > 0) {
            $violationsString = (string)$violations;
            throw new InvalidArgumentException($violationsString);
        }
    }

    protected function responseFileToDownload(
        string  $fileName,
        string  $disposition = 'attachment',
        bool    $deleteFile = true,
        ?string $path = null
    ): Response
    {

        $pathDirectory = (null === $path) ? $this->getParameter('upload_dir') : realpath($path);
        $pathWithFile = $pathDirectory . '/' . $fileName;
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
        $response = new BinaryFileResponse($pathWithFile);
        // Set the mimetype with the guesser or manually
        if ($mimeTypeGuesser->isGuesserSupported()) {
            // Guess the mimetype of the file according to the extension of the file
            $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($pathWithFile));
        } else {
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $response->headers->set('Content-Type', 'text/plain');
        }
        // Set content disposition inline of the file
        $response->setContentDisposition(
            $disposition,
            $fileName
        );
        $response->deleteFileAfterSend($deleteFile);
        return $response;


    }

    protected function streamPdf(ReportGeneratorInterface $pdf, array $disposition = ['Atttachment' => false]): Response
    {
        return new Response(
            $pdf->stream($disposition),
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/pdf'
            ]

        );

    }

    protected function getUserSession(): UserInterface
    {

        $user = $this->getUser();
        if (null === $user) {
            throw new RuntimeException('UserSession not available!!!');
        }

        return $user;

    }

    protected function storeInSession(Request $request, string $key, mixed $value): void
    {

        $this->setSession($request);
        $this->defaultSession->set($key,$value);
    }

    protected function getFromSession(Request $request, string $key): mixed
    {

        $this->setSession($request);
        return $this->defaultSession->get($key);
    }

    protected function getFromSessionOrFail(Request $request, string $key)
    {

        $value = @$this->getFromSession($request, $key);
        if (null === $value) {
            throw new RuntimeException(sprintf('Value:%s, does not exists in Session', $key));
        }

        return $value;

    }


    private function setSession(Request $request):void{
        if(null === $this->defaultSession){
            $this->defaultSession = $request->getSession();
        }
    }


    protected function showSuccessfullOperation(string $message = ''):Response{


        $user = $this->getUserSession();
        /**
         * @var Profile $profile
         */

        return $this->render('Messages/success_operation.html.twig',[
            'message' => $message,
            'baseHtml' => $user->profile()->baseTemplate
        ]);

    }


	public function showErrorPage(Throwable $ex): Response
	{

		return $this->render('Messages/error_operation.html.twig', [
			'exception' => $ex,
		]);

	}

	public function show404(string $message = ''):Response{

		return $this->render('Messages/error_404.html.twig', [
			'message' => $message,
		],new Response('',404));

	}


}