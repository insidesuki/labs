<?php

namespace Shared\Application\Service\Log;

use Error;
use Psr\Log\LoggerInterface;
use Throwable;

trait LoggerTrait
{

	private array $errors = [];
	private array $infos = [];
	private array $criticals = [];

	public function __construct(
		private readonly LoggerInterface $logger
	)
	{
	}

	public function info(string $info, array $context = []): void
	{

		$this->logger->info($info, $context);
		$this->infos[] = [
			'info' => $info,
			'context' => $context
		];
	}


	public function error(string $tag, Throwable $throwable): void
	{

		$details = [
			'message' => $throwable->getMessage(),
			'file' => $throwable->getFile(),
			'line' => $throwable->getLine()
		];
		$this->logger->error($tag, $details);

		$this->errors[] = [
			'tag' => $tag,
			'detail' => $details
		];

	}

	public function critical(string $tag, Error $error): void
	{
		$details = [
			'message' => $error->getMessage(),
			'file' => $error->getFile(),
			'line' => $error->getLine()
		];

		$this->logger->critical($tag, $details);
		$this->criticals[] = [
			'tag' => $tag,
			'detail' => $details
		];
	}


}
