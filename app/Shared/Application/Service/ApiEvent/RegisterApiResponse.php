<?php
declare(strict_types = 1);

namespace Shared\Application\Service\ApiEvent;
use Shared\Domain\Command\ApiResponseCommandInterface;
use Shared\Domain\Model\ApiEvent;
use Shared\Domain\Repository\ApiEventRepositoryInterface;

class RegisterApiResponse
{

	public function __construct(private readonly ApiEventRepositoryInterface $apiEventRepository){}

	public function handle(string $idApiEvent, ApiResponseCommandInterface $responseCommand): ApiEvent
	{

		$apiEvent = $this->apiEventRepository->findApiEvent($idApiEvent);
		$apiEvent->responseEvent($responseCommand);
		return $this->apiEventRepository->store($apiEvent);

	}

}