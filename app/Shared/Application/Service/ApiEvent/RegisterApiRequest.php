<?php
declare(strict_types=1);

namespace Shared\Application\Service\ApiEvent;

use Shared\Domain\Command\ApiRequestCommandInterface;
use Shared\Domain\Model\ApiEvent;
use Shared\Domain\Repository\ApiEventRepositoryInterface;

class RegisterApiRequest
{
	public function __construct(
		private readonly ApiEventRepositoryInterface $eventRepository
	)
	{
	}

	public function handle(ApiRequestCommandInterface $apiRequestCommand): ApiEvent
	{

		$apiEvent = ApiEvent::requestEvent($apiRequestCommand);
		$this->eventRepository->store($apiEvent);
		return $apiEvent;

	}

}