<?php
declare(strict_types=1);

namespace Security\Infrastructure\Subscriber;

use Security\Application\User\SendActivation\SendActivation;
use Security\Domain\User\Event\UserWasCreated;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserWasCreatedSubscriber implements EventSubscriberInterface
{

	public function __construct(private SendActivation $sendActivation){}

	public static function getSubscribedEvents(): array
	{
		return [
			UserWasCreated::NAME => [
				['onUserCreated', 0]
			]
		];
	}

	public function onUserCreated(UserWasCreated $event):void
	{

		$this->sendActivation->handle($event->idUser);
	}

}