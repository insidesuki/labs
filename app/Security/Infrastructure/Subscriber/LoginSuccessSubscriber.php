<?php
declare(strict_types = 1);

namespace Security\Infrastructure\Subscriber;


use Security\Application\Recovery\CreateTokenRecoveryPassword;
use Security\Domain\Log\Entity\Log;
use Security\Domain\Log\Repository\LogRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;

class LoginSuccessSubscriber implements EventSubscriberInterface
{



    public function __construct(
		private readonly TokenStorageInterface $tokenStorage,
        private readonly LogRepository $logRepository,
        private readonly UrlGeneratorInterface       $urlGenerator,
        private readonly CreateTokenRecoveryPassword $createTokenRecoveryPassword
	){}


	public static function getSubscribedEvents(): array
	{
		return [
			LoginSuccessEvent::class => 'onLoginSuccess'
		];
	}

	/**
	 * @throws \JsonException
	 */
	public function onLoginSuccess(LoginSuccessEvent $event): ?Response
    {

		$request = $event->getRequest();
		$user    = $event->getUser();
        $currentSession = $request->getSession();

        // check user expiration
        if ($this->checkUserExpiration($user)) {
            $event->setResponse(new RedirectResponse('expired'));
            return $event->getResponse();
        }

        // register login success
        $this->registerLoginSuccess($request,$user);

        // redirect
        $firewallName = $event->getFirewallName();
        $urlDestination = $currentSession->get('_security.' . $firewallName . '.target_path');
        if(null === $urlDestination) {
            $urlDestination = $this->urlGenerator->generate($user->profile()->defaultRoute());
        }

         $event->setResponse(
            new RedirectResponse($urlDestination)
        );

        return $event->getResponse();


	}


    private function checkUserExpiration(UserInterface $user): bool
	{

        if ($user->mustBeRenewed()) {

            // set token user to null (logout)
            $token = $this->tokenStorage->getToken();
            if ($token) {
                $this->tokenStorage->setToken();
            }

            $this->createTokenRecoveryPassword->handle($user->email());
            return true;

        }

        return false;


    }

    private function registerLoginSuccess(Request $request,UserInterface $user):void
    {
        $userAgent = $request->server->get('HTTP_USER_AGENT') ?? '';
        $ipAddress = $request->server->get('REMOTE_ADDR');

        $loginLog = Log::createLoginSuccessfull($user->idUser(),$ipAddress,$userAgent);

        $this->logRepository->store($loginLog);


    }


}