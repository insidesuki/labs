<?php
declare(strict_types = 1);

namespace Security\Infrastructure\Subscriber;

use Security\Application\Login\ManageBadLogin;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Event\LoginFailureEvent;
use SymfonyCasts\Bundle\ResetPassword\Exception\TooManyPasswordRequestsException;

class LoginFailSubscriber implements EventSubscriberInterface
{

	public function __construct(private ManageBadLogin $manageBadLogin){}

	/**
	 * @inheritDoc
	 */
	public static function getSubscribedEvents()
	{
		return [
			LoginFailureEvent::class => 'onLoginFail'
		];
	}

	public function onLoginFail(LoginFailureEvent $event)
	{

		try{

			$username = $event->getRequest()->get('username');

			// exlude oper api username
			if(null === $username){
				return;
			}

			$userAgent = $event->getRequest()->server->get('HTTP_USER_AGENT') ?? '';
			$remoteIp = $event->getRequest()->server->get('REMOTE_ADDR');

			if($this->manageBadLogin->handle($username,$remoteIp,$userAgent)){

				$event->setResponse(new RedirectResponse('blocked'));

			}
		}
		catch(TooManyPasswordRequestsException){
			$event->setResponse(new RedirectResponse('blocked'));
		}



	}
}