<?php
declare(strict_types=1);

namespace Security\Infrastructure\Runtime;

use Symfony\Component\Security\Core\User\UserInterface;

class Security
{
    private ?UserInterface $user;

    public function __construct(private readonly \Symfony\Component\Security\Core\Security $security)
    {
        $this->user = $this->security->getUser();

    }

    public function user()
    {
        return $this->user;
    }


}