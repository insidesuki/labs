<?php
declare(strict_types=1);

namespace Security\Infrastructure\Console;

use Exception;

use Security\Application\User\LoginLink\CreateLoginLink;
use Security\Domain\User\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\MailerInterface;

class CreateLoginLinkConsole extends Command
{
    protected static $defaultName = 'security:loginlink:create';
    protected static $defaultDescription = 'Create a LoginLink for a user username required ';

    public function __construct(
        private readonly MailerInterface   $mailer,
        private readonly CreateLoginLink $createLoginLink,
        private readonly UserRepository $userRepository

    )
    {
        parent::__construct();
    }

    public function configure()
    {

        $this->addArgument('username',InputArgument::REQUIRED,'username required!!!');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $username = $input->getArgument('username');
            $ui = new SymfonyStyle($input, $output);

            $user = $this->userRepository->searchByUsername($username);

            if(null === $user){
                $ui->error(sprintf('User with username:%s, not found!!!',$username));
            }
            else{
                $loginLink = $this->createLoginLink->handle($user->idUser());
                $ui->title('Your LoginLink');
                $ui->success($loginLink->getUrl());

            }




        }
        catch (Exception $ex)
        {

            dd($ex);



        }
        return Command::SUCCESS;
    }

}