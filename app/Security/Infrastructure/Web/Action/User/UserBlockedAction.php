<?php
declare(strict_types=1);

namespace Security\Infrastructure\Web\Action\User;

use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\HttpFoundation\Response;

class UserBlockedAction extends AbstractCommonAction
{
    public function __invoke(): Response{


        return $this->render('Security/UserBlocked.twig');


    }


}