<?php
declare(strict_types = 1);

namespace Security\Infrastructure\Web\Action\Reset;

use Security\Application\Recovery\CreateTokenRecoveryPassword;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;

/**
 * Reset password start, show form to catch recovery email
 */
class RequestPasswordAction extends AbstractController
{
	use ResetPasswordControllerTrait;

	public function __construct(private CreateTokenRecoveryPassword $resetPassword){}

	/**
	 * @throws ResetPasswordExceptionInterface
	 */
	public function __invoke(Request $request): Response
	{

        if($request->isMethod('POST')){

            $token = $this->resetPassword->handle($request->request->get('email'));

            if(false !== $token) {
                $this->setTokenObjectInSession($token);

            }
            return $this->redirectToRoute('app_check_email');

        }

			return $this->render('ResetPassword/RequestEmailForReset.html.twig');
		


	}


}