<?php
declare(strict_types = 1);

namespace Security\Infrastructure\Web\Action\Reset;

use Security\Application\Recovery\ChangeUserPassword;
use Security\Infrastructure\Form\ChangePasswordFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

/**
 * Validates and process the reset URL that the user clicked in their email.
 */
class ResetPasswordAction extends AbstractController
{

	use ResetPasswordControllerTrait;

	public function __construct(
		private ResetPasswordHelperInterface $resetPasswordHelper,
		private ChangeUserPassword $changeUserPassword
	){}

	public function __invoke(?string $token, Request $request): Response
	{

		if($token) {

			// We store the token in session and remove it from the URL, to avoid the URL being
			// loaded in a browser and potentially leaking the token to 3rd party JavaScript.
			$this->storeTokenInSession($token);
			return $this->redirectToRoute('app_reset_password');

		}

		$token = $this->getTokenFromSession();

		if(null === $token) {
			throw $this->createNotFoundException('No reset password token found in the URL or in the session.');
		}

		try {
			$user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
		}
		catch (ResetPasswordExceptionInterface $e) {

			$this->addFlash('reset_password_error', sprintf(
				'%s - %s',
				ResetPasswordExceptionInterface::MESSAGE_PROBLEM_VALIDATE,
				$e->getReason()
			));

			return $this->redirectToRoute('app_forgot_password_request');

		}


		return $this->handleForm($request, $token, $user->idUser());


	}

	private function handleForm(Request $request, string $token, string $idUser)
	{

		$form = $this->createForm(ChangePasswordFormType::class);
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()) {

			// A password reset token should be used only once, remove it.
			$this->resetPasswordHelper->removeResetRequest($token);
			$this->changeUserPassword->handle($idUser, $form->get('plainPassword')->getData());

			$this->cleanSessionAfterReset();

			return $this->redirectToRoute('app_login');



		}

		return $this->render('ResetPassword/SetPassword.html.twig', [
			'resetForm' => $form->createView(),
		]);


	}


}