<?php
declare(strict_types = 1);

namespace Security\Infrastructure\Storage\Doctrine\Repository;

use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use Security\Domain\Profile\Entity\Profile;
use Security\Domain\Profile\Exception\ProfileDoesNotExistsException;
use Security\Domain\Profile\Repository\ProfileRepository;

class ProfileDoctrineRepository extends AbstractDoctrineRepository implements ProfileRepository
{


	protected static function entityClass(): string
	{
		return Profile::class;
	}

	public function searchByProfile(string $profileName): ?Profile
	{
		return $this->objectRepository->findOneBy(['profile' => $profileName]);
	}

	public function findByProfile(string $profileName): Profile
	{
		$profile = $this->searchByProfile($profileName);

		if(null === $profile){
			throw new ProfileDoesNotExistsException($profileName);
		}

		return $profile;
	}

    public function store(Profile $profile): Profile
	{

		$this->saveEntity($profile,true);

        return $profile;
	}

	public function findAll(): array
	{
		return $this->objectRepository->findAll();
	}
}