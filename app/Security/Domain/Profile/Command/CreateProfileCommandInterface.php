<?php

namespace Security\Domain\Profile\Command;

interface CreateProfileCommandInterface
{

	public function profile(): string;

	public function defaultRoute(): string;

    public function baseTemplate():string;

}