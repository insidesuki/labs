<?php
declare(strict_types = 1);

namespace Security\Domain\Profile\Entity;

use Security\Domain\Profile\Command\CreateProfileCommandInterface;

class Profile
{

    private string $defaultRoute;

    private string $baseTemplate;

    private function __construct(
        public readonly string $profile,
        string $defaultRoute,
        string $baseTemplate
    )
	{
        $this->defaultRoute = $defaultRoute;
        $this->baseTemplate = $baseTemplate;
    }


	public static function create(CreateProfileCommandInterface $command): Profile
	{

		return new self($command->profile(), $command->defaultRoute(),$command->baseTemplate());
	}


	public function __toString(): string
	{
		return $this->profile;
	}

    public function defaultRoute(): string
    {
        return $this->defaultRoute;
    }

    public function baseTemplate(): string
    {
        return $this->baseTemplate;
    }




}