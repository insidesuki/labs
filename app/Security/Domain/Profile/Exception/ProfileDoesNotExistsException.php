<?php
declare(strict_types = 1);

namespace Security\Domain\Profile\Exception;

use RuntimeException;

class ProfileDoesNotExistsException extends RuntimeException
{

	public function __construct(string $name)
	{
		parent::__construct(sprintf('Profile:%s does not exists!!',$name));
	}
}