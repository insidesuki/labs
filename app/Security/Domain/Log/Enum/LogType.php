<?php

namespace Security\Domain\Log\Enum;

enum LogType:string
{

    case loginSuccess = 'loginSuccess';

    case loginError = 'loginError';

}
