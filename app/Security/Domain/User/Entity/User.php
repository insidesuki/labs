<?php

namespace Security\Domain\User\Entity;

use DateTime;
use DateTimeImmutable;
use Insidesuki\DDDUtils\Domain\Event\DomainEventPublisher;
use Security\Domain\Log\Repository\LogRepository;
use Security\Domain\Profile\Entity\Profile;
use Security\Domain\User\Command\CreateUserCommandInterface;
use Security\Domain\User\Event\UserWasCreated;
use Shared\Domain\Model\ValueObject\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    private const EXCLUDED_PROFILES_SECURITY = [
        'api'
    ];

    private const PASSWORD_EXPIRATION_DAYS = 90;
    private const MAX_LOGIN_ATTEMPTS = 5;

	private string  $idUser;
	private string  $username;
	private string  $name;
	private string  $password;
	private string  $email;
	private bool    $active = false;
	private array   $roles  = [];
	private Profile $profile;

    private DateTimeImmutable $lastPasswordVerification;

	private bool $isVerified = false;

	private function __construct(

		string $name,
		string $username,
		string $email,
		string $role,
		Profile $profile
	)
	{
		$this->idUser   = Uuid::v4()->toRfc4122();
		$this->name     = $name;
		$this->username = $username;
		$this->email    = $email;
		$this->roles[]  = $role;
		$this->profile  = $profile;
        $this->lastPasswordVerification = new DateTimeImmutable();

		// raise event
		DomainEventPublisher::instance()->raise(
			new UserWasCreated(
				$this->idUser,$this->email
			)
		);


	}

	/**
	 * @param CreateUserCommandInterface $registerUserCommand
	 * @param UserPasswordHasherInterface $userPasswordHasher
	 * @return static
	 */
	public static function createNewUser(
        CreateUserCommandInterface  $registerUserCommand,
        UserPasswordHasherInterface $userPasswordHasher,
        Profile $profile
    ): self
	{

		$emailAddress = Email::create($registerUserCommand->email());

		$newUser = new self(
			$registerUserCommand->name(),
			$registerUserCommand->username(),
			$emailAddress->emailAddress,
			'ROLE_'.strtoupper($profile->profile),
			$profile
		);
		// hash password
		$newUser->password = $userPasswordHasher->hashPassword(
			$newUser,
            uniqid('Ux86789_dsh'.random_int(0,9999), true)
		);

		return $newUser;

	}


    public function mustBeRenewed(): bool
    {

        if(in_array($this->profile->profile,self::EXCLUDED_PROFILES_SECURITY)){
            return false;
        }

        // if is zero, nothing happens
        if(self::PASSWORD_EXPIRATION_DAYS === 0) {
            return true;
        }

        $today = new DateTime();
        $passwordDaysExpiration =date_diff($this->lastPasswordVerification, $today)->format('%a');

        return $passwordDaysExpiration >= self::PASSWORD_EXPIRATION_DAYS;


    }

    public function desactivate(): void
    {

        $this->active = false;

    }

    public function activate(): void
    {

        $this->active = true;
		$this->isVerified = true;
    }


    public function mustBeBlocked(LogRepository $logRepository): bool
    {

        if (in_array($this->profile->profile, self::EXCLUDED_PROFILES_SECURITY)) {
            return false;
        }


        // if is zero, nothing happens
        if (self::MAX_LOGIN_ATTEMPTS === 0) {
            return false;
        }

        $loginAttempt = $logRepository->countRecentLoginAttemptsUser($this);



        if ($loginAttempt > self::MAX_LOGIN_ATTEMPTS) {

           $this->desactivate();
            return true;

        }

        return false;


    }


	public function __toArray():array{

		return [
			'idUser' => $this->idUser,
			'name' => $this->name,
			'username' => $this->username,
			'profile' => $this->profile()->profile,
			'email' => $this->email,
			'active' => $this->isActive()
		];


	}



    public function lastPasswordVerification(): DateTimeImmutable
    {
        return $this->lastPasswordVerification;
    }



    /**
	 * @return string
	 */
	public function name(): string
	{
		return $this->name;
	}

	/**
	 * @deprecated since Symfony 5.3, use getUserIdentifier instead
	 */
	public function username(): string
	{
		return $this->username;
	}

	public function email(): ?string
	{
		return $this->email;
	}

	/**
	 * @param string $plainPassword
	 * @param UserPasswordHasherInterface $userPasswordHasher
	 * @return void
	 */
	public function changePassword(string $plainPassword, UserPasswordHasherInterface $userPasswordHasher)
	{

		$this->password = $userPasswordHasher->hashPassword(
			$this,
			$plainPassword
		);
        $this->lastPasswordVerification = new DateTimeImmutable();
        $this->active = true;

	}

	public function idUser(): string
	{
		return $this->idUser;
	}

	/**
	 * A visual identifier that represents this user.
	 * @see UserInterface
	 */
	public function getUserIdentifier(): string
	{
		return (string) $this->username;
	}

	/**
	 * @see UserInterface
	 */
	public function getRoles(): array
	{
		$roles = $this->roles;
		//// guarantee every user at least has ROLE_USER
		//$roles[] = 'ROLE_USER';

		return array_unique($roles);
	}



	/**
	 * @see PasswordAuthenticatedUserInterface
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword(string $password): self
	{
		$this->password = $password;
		return $this;
	}



	/**
	 * @see UserInterface
	 */
	public function eraseCredentials()
	{
		// If you store any temporary, sensitive data on the user, clear it here
		// $this->plainPassword = null;
	}

	/**
	 * @return bool
	 */
	public function isActive(): bool
	{
		return $this->active;
	}


	public function isVerified(): bool
	{
		return $this->isVerified;
	}


	public function profile(): Profile
	{
		return $this->profile;
	}


    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }
}
