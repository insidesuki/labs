<?php

namespace Security\Domain\User\Command;

interface CreateUserCommandInterface
{

	public function name(): string;

	public function username(): string;

	public function email(): string;

	public function profile(): string;


}