<?php
declare(strict_types = 1);

namespace Security\Domain\User\Event;

use DateTimeImmutable;
use Insidesuki\DDDUtils\Domain\Event\Contracts\EventPersistibleInterface;

class UserWasCreated implements EventPersistibleInterface
{


	public const NAME = 'user.was.created';
	public readonly DateTimeImmutable $ocurredOn;

	public function __construct(
		public readonly string $idUser,
		public readonly string $email
	)
	{
		$this->ocurredOn = new DateTimeImmutable();
	}


	public function occurredOn(): DateTimeImmutable
	{
		return $this->ocurredOn;
	}

	public function name(): string
	{
		return self::NAME;
	}
}