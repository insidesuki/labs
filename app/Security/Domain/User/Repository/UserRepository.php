<?php

namespace Security\Domain\User\Repository;


use Security\Domain\User\Entity\User;

interface UserRepository
{

	public function findAll():array;

	public function findByIdUser(string $idUser): User;

	public function searchByEmail(string $email): ?User;

    public function searchByUsername(string $username): ?User;

	public function store(User $user): User;
}