<?php
declare(strict_types=1);

namespace Security\Application\Login;

use Security\Domain\Log\Repository\LogRepository;
use Security\Domain\User\Entity\User;
use Security\Domain\User\Repository\UserRepository;

class ManageBadLogin
{

    public function __construct(
        private readonly LogRepository      $logRepository,
        private readonly UserRepository     $userRepository,
        private readonly RegisterLoginError $registerLoginError
    )
    {
    }

    public function handle(string $username, string $ipAddress, string $userAgent): bool
    {

        $user = $this->userRepository->searchByUsername($username);
        $userLogIdentifier = (null === $user) ? $username : $user->idUser();


        if(($user instanceof User) && $user->mustBeBlocked($this->logRepository)) {

            $this->userRepository->store($user);
            return true;
        }

        $this->registerLoginError->handle($userLogIdentifier, $ipAddress, $userAgent);
        return false;

    }



}