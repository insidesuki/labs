<?php
declare(strict_types=1);

namespace Security\Application\Profile\CreateProfile\Command;

use Insidesuki\DDDUtils\Domain\DtoSerializer;
use Security\Domain\Profile\Command\CreateProfileCommandInterface;

class CreateProfileCommand extends DtoSerializer implements CreateProfileCommandInterface
{


    private string $profile;

    private string $defaultRoute;
    private string $baseTemplate;

    public function __construct(string $profile, string $defaultRoute,string $defaultTemplate)
    {
        $this->profile = $profile;
        $this->defaultRoute = $defaultRoute;
        $this->baseTemplate = $defaultTemplate;
    }

    public function profile(): string
    {
        return $this->profile;
    }

    public function defaultRoute(): string
    {
        return $this->defaultRoute;
    }

    public function baseTemplate(): string
    {
        return $this->baseTemplate;
    }

}