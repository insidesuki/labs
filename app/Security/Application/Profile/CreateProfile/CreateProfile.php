<?php
declare(strict_types=1);

namespace Security\Application\Profile\CreateProfile;

use Security\Domain\Profile\Command\CreateProfileCommandInterface;
use Security\Domain\Profile\Entity\Profile;
use Security\Domain\Profile\Repository\ProfileRepository;

class CreateProfile
{

    public function __construct(
        private readonly ProfileRepository $profileRepository
    ){}

    public function handle(CreateProfileCommandInterface $createProfileCommand):Profile{

        $profile = $this->profileRepository->searchByProfile($createProfileCommand->profile());

        if(null === $profile){
            $profile = Profile::create($createProfileCommand);
            $this->profileRepository->store($profile);
        }
        return $profile;
    }

}