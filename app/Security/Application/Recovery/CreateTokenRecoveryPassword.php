<?php
declare(strict_types = 1);

namespace Security\Application\Recovery;

use Security\Domain\User\Entity\User;
use Security\Domain\User\Repository\UserRepository;
use Security\Infrastructure\Storage\Doctrine\Repository\ResetPasswordRequestRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

class CreateTokenRecoveryPassword
{

	private ?User $user;

	public function __construct(
		private ResetPasswordHelperInterface $resetPasswordHelper,
		private UserRepository $userRepository,
        private ResetPasswordRequestRepository $passwordRequestRepository,
		private MailerInterface $mailer
	){}

	/**
	 * @throws ResetPasswordExceptionInterface
	 */
	public function handle(string $userEmail): false|ResetPasswordToken
	{


		$this->user = $this->userRepository->searchByEmail($userEmail);


		// Do not reveal whether a user account was found or not.
		if($this->user instanceof User) {

            // remove existed password request for user
            $this->passwordRequestRepository->removeUserPasswordRequest($this->user);

			// create reset token
			$resetToken = $this->resetPasswordHelper->generateResetToken($this->user);

			$this->sendRecoveryEmail($resetToken);
			return $resetToken;

		}

		return false;

	}

	private function sendRecoveryEmail(ResetPasswordToken $resetToken):void
	{

		$emailTemplate = new TemplatedEmail();
		$emailTemplate->from(
			new Address('sys@iprprevencion.es', 'AccountRecovery')
		);
		$emailTemplate->to($this->user->email());
		$emailTemplate->subject('Your password reset request');
		$emailTemplate->htmlTemplate('ResetPassword/RecoveryEmailTemplate.html.twig');
		$emailTemplate->context(
			[
				'resetToken' => $resetToken
			]
		);

		$this->mailer->send($emailTemplate);


	}

}