<?php

namespace Security\Application\User\ListUser;

interface UsersPresenter
{

	public function write(array $userCollection): void;

	public function read(): mixed;


}