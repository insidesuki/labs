<?php
declare(strict_types = 1);

namespace Security\Application\User\SendActivation;

use Security\Domain\User\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

class SendActivation
{


	public function __construct(
		private readonly ResetPasswordHelperInterface $resetPasswordHelper,
		private readonly MailerInterface $mailer,
		private readonly UserRepository $userRepository
	){}

	/**
	 * @throws ResetPasswordExceptionInterface
	 * @throws TransportExceptionInterface
	 */
	public function handle(string $idUser): ResetPasswordToken
	{

		$user = $this->userRepository->findByIdUser($idUser);
		$resetToken = $this->resetPasswordHelper->generateResetToken($user);

		$emailTemplate = new TemplatedEmail();
		$emailTemplate->from(
			new Address('sys@iprprevencion.es', 'Welcome')
		);
		$emailTemplate->to($user->email());
		$emailTemplate->subject('Your account is almost active');
		$emailTemplate->htmlTemplate('Registration/PasswordChoice.html.twig');
		$emailTemplate->context(
			[
				'resetToken' => $resetToken,
				'username' => $user->getUserIdentifier(),
				'name' => $user->name()
			]
		);

		$this->mailer->send($emailTemplate);

		return $resetToken;


	}


}