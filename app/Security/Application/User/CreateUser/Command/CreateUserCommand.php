<?php
declare(strict_types = 1);

namespace Security\Application\User\CreateUser\Command;

use Insidesuki\DDDUtils\Domain\DtoSerializer;
use Security\Domain\User\Command\CreateUserCommandInterface;

class CreateUserCommand extends DtoSerializer implements CreateUserCommandInterface
{

	private string $name;
	private string $username;
	private string $email;
	private string $profile;

    public function __construct(
        string $name,
        string $username,
        string $email,
        string $profile)
    {
        $this->name = $name;
        $this->username = $username;
        $this->email = $email;
        $this->profile = $profile;
    }


	public function name(): string
	{
		return $this->name;
	}


	public function username(): string
	{
		return $this->username;
	}


	public function email(): string
	{
		return $this->email;
	}




	public function profile(): string
	{
		return $this->profile;
	}


}