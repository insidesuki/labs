<?php
declare(strict_types=1);

namespace Security\Application\User\LoginLink;

use Security\Domain\User\Repository\UserRepository;
use Symfony\Component\Security\Http\LoginLink\LoginLinkDetails;
use Symfony\Component\Security\Http\LoginLink\LoginLinkHandlerInterface;

class CreateLoginLink
{

    public function __construct(
        private readonly LoginLinkHandlerInterface $loginLinkHandler,
        private readonly UserRepository            $userRepository
    )
    {
    }

    public function handle(string $idUser): LoginLinkDetails
    {
        $user = $this->userRepository->findByIdUser($idUser);
        $loginLink = $this->loginLinkHandler->createLoginLink($user);

        $url = $loginLink->getUrl();
        $urlParts = explode('&hash=', $url);

        return $loginLink;

    }

}