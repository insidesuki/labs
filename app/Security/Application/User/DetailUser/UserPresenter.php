<?php

namespace Security\Application\User\DetailUser;

use Security\Domain\User\Entity\User;

interface UserPresenter
{

	public function write(User $user): void;

	public function read(): mixed;

}