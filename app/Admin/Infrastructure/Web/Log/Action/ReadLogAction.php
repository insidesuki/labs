<?php
declare(strict_types=1);

namespace Admin\Infrastructure\Web\Log\Action;

use Exception;
use Insidesuki\Utilities\File\Folder;
use Shared\Application\Service\Log\ParseLog;
use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReadLogAction extends AbstractCommonAction
{


	public function __construct(
		private readonly ParseLog $parseLog
	)
	{
	}


	public function __invoke(Request $request): Response
	{
		try {
			$logDir = $this->getParameter('kernel.logs_dir') . '/';
			$logFile = $request->query->get('logFile');

			$logs = [];
			if(null !== $logFile){
				$logFullPath = $logDir.$logFile;
				$logs = $this->parseLog->handle($logFullPath);
			}
			return $this->render('log_reader.html.twig', [
				'logs' => $logs,
				'logFiles' => Folder::fromPath($logDir)->realFiles(),
				'logFile' => $logFile
			]);
		} catch (Exception $exception) {
			return $this->showErrorPage($exception);
		}
	}



}