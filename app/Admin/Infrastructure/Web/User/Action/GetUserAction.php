<?php
declare(strict_types=1);

namespace Admin\Infrastructure\Web\User\Action;

use Admin\Infrastructure\Web\User\Presentation\UserArrayPresenter;
use Security\Application\User\DetailUser\RetrieveUser;
use Security\Domain\User\Exception\UserDoesNotExistsException;
use Shared\Infrastructure\Web\Action\AbstractCommonAction;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class GetUserAction extends AbstractCommonAction
{


	public function __construct(
		private readonly RetrieveUser $retrieveUser
	)
	{
	}

	public function __invoke(string $idUser):Response{


		try{

			$userPresentation = $this->retrieveUser->handle($idUser,new UserArrayPresenter());
			return $this->render('user_detail.html.twig',[
				'user' => $userPresentation->read()
			]);


		}
		catch(UserDoesNotExistsException){

			return $this->show404();
		}
		catch(Throwable $throwable){
			return $this->showErrorPage($throwable);
		}


	}


}