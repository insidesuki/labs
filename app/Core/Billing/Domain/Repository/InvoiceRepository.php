<?php

namespace Billing\Domain\Repository;

use Billing\Domain\Entity\Invoice;

interface InvoiceRepository
{

	public function findInvoice(string $id):Invoice;

	public function store(Invoice $invoice):Invoice;

}