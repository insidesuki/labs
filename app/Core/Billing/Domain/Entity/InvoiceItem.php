<?php
declare(strict_types=1);

namespace Billing\Domain\Entity;

class InvoiceItem
{

	private string $id;

	private Invoice $invoice;

	private int $amount;

	public function id(): string
	{
		return $this->id;
	}

	public function invoice(): Invoice
	{
		return $this->invoice;
	}

	public function amount(): int
	{
		return $this->amount;
	}




}