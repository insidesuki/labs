<?php
declare(strict_types=1);

namespace Billing\Domain\Entity;

use Shared\Domain\Model\ValueObject\Amount;

class Invoice
{

	public string $id;

	public string $number;

	public Amount $total;


	public function id(): string
	{
		return $this->id;
	}

	public function number(): string
	{
		return $this->number;
	}

	public function total(): Amount
	{
		return $this->total;
	}


	public function increment(int $increment):void{

		$this->total = $this->total->increment($increment);

	}




}