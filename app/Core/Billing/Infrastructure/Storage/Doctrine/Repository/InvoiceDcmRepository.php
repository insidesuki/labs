<?php
declare(strict_types=1);

namespace Billing\Infrastructure\Storage\Doctrine\Repository;

use Billing\Domain\Entity\Invoice;
use Billing\Domain\Repository\InvoiceRepository;
use Insidesuki\DDDUtils\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;

class InvoiceDcmRepository extends AbstractDoctrineRepository implements InvoiceRepository
{

	/**
	 * @inheritDoc
	 */
	protected static function entityClass(): string
	{
		return Invoice::class;
	}

	public function findInvoice(string $id): Invoice
	{
		return $this->findById($id);
	}

	public function store(Invoice $invoice): Invoice
	{
		$this->saveEntity($invoice);
		return $invoice;
	}
}